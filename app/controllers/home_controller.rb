class HomeController < ApplicationController
  require 'net/http'

  def index
  end

  def projects
    uri = URI(ECO_URL + 'search_with_tags.json?tags=match&object_type=project')
    res = Net::HTTP.get(uri)
    if valid_json?(res) && !res.include?("[]")
      @projects = JSON.parse(res)
    else
      @projects = []
    end
  end

  def people
    uri = URI(ECO_URL + 'search_with_tags.json?tags=match&object_type=user')
    res = Net::HTTP.get(uri)
    if valid_json?(res) && !res.include?("[]")
      @people = JSON.parse(res)
    else
      @people = []
    end

    @filter_tags = [
      "css",
      "java",
      "html",
      "c",
      "c#",
      "python",
      "ruby",
    ]

  end

  private

  def valid_json?(string)
    begin
      !!JSON.parse(string)
    rescue JSON::ParserError
      false
    end
  end
end
