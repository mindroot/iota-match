class ApplicationController < ActionController::Base

  ECO_URL = Rails.env.development? ? "https://ecosystem-dev.iota.cafe/" : "https://ecosystem.iota.org/"
  #ECO_URL = "https://ecosystem-dev.iota.cafe/"
end
