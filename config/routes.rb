Rails.application.routes.draw do
  root 'home#index'
  get '/projects', to: 'home#projects'
  get '/people', to: 'home#people'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
